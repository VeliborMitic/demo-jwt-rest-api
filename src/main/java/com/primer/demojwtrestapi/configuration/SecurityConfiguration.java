package com.primer.demojwtrestapi.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.primer.demojwtrestapi.security.JsonWebTokenProvider;
import com.primer.demojwtrestapi.security.filter.JwtAuthenticationFilter;
import com.primer.demojwtrestapi.security.filter.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)

public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private UserDetailsService userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private JsonWebTokenConfiguration configuration;
    private JwtAuthorizationFilter jwtAuthorizationFilter;
    private ObjectMapper objectMapper;
    private JsonWebTokenProvider jsonWebTokenProvider;


    @Autowired
    public SecurityConfiguration(@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService,
                                 BCryptPasswordEncoder bCryptPasswordEncoder,
                                 JsonWebTokenConfiguration configuration,
                                 JwtAuthorizationFilter jwtAuthorizationFilter,
                                 ObjectMapper objectMapper,
                                 JsonWebTokenProvider jsonWebTokenProvider
    ) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.configuration = configuration;
        this.jwtAuthorizationFilter = jwtAuthorizationFilter;
        this.objectMapper = objectMapper;
        this.jsonWebTokenProvider = jsonWebTokenProvider;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtAuthenticationFilter(
                    objectMapper,
                    jsonWebTokenProvider,
                    authenticationManager(),
                    configuration))
                .addFilterAfter(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, configuration.getEntryPoint()).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/v2/api-docs",
                        "/webjars/**",
                        "/swagger-resources/**",
                        "/configuration/**",
                        "/*.html"
//                        ,"/api/v1/categories/**",
//                        "/api/v1/products/**"
                ).permitAll()
                .anyRequest().authenticated();
    }
}