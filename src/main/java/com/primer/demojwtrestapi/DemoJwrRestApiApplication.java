package com.primer.demojwtrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJwrRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoJwrRestApiApplication.class, args);
    }

}
