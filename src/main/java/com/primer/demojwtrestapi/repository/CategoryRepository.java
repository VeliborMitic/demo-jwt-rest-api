package com.primer.demojwtrestapi.repository;

import com.primer.demojwtrestapi.entity.Category;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CategoryRepository extends BaseRepository<Category>{

    Optional<Category> findByName(String name);

    @Query("SELECT (COUNT(c) > 0) FROM Category c WHERE (c.name = :myName) AND c.id <> :myId")
    boolean existsByNameAndDifferentId(String myName, Integer myId);

}
