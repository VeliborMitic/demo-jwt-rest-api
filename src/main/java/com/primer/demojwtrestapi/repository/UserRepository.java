package com.primer.demojwtrestapi.repository;

import com.primer.demojwtrestapi.entity.User;

public interface UserRepository extends BaseRepository<User> {
}
