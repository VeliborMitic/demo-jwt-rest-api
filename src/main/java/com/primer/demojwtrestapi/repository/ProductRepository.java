package com.primer.demojwtrestapi.repository;

import com.primer.demojwtrestapi.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends BaseRepository<Product>{

    Optional<Product> findByName(String name);

    List<Product> findAllByCategoryId(Integer id);

    Page<Product> findAllByCategoryId(Integer id, Pageable pageable);
}
