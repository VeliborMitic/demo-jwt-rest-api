package com.primer.demojwtrestapi.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Column(unique = true)
    @Size(max = 50)
    private String email;

    @Size(min = 6)
    private String password;

    @Size(max = 25)
    private String name;

    @ManyToOne
    private Role role;
}
