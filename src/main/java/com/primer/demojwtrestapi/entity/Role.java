package com.primer.demojwtrestapi.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "roles")
public class Role extends BaseEntity {

    @Size(max = 25)
    private String name;

}