package com.primer.demojwtrestapi.model.category;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryPostModel extends CategoryBaseModel{
}
