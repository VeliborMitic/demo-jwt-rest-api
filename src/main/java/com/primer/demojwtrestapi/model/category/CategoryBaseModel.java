package com.primer.demojwtrestapi.model.category;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CategoryBaseModel {

    @NotEmpty
    @Size(min = 3, max = 20, message = "Category name can not be empty - Minimum 3, maximum 20 characters")
    private String name;
}
