package com.primer.demojwtrestapi.model.product;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ProductBaseModel {

    @NotBlank(message = "Product name can not be empty")
    @Size(min = 3, max = 30, message = "Product name must be between 3 and 30 characters long")
    private String name;

    @Min(value = 1, message = "Category Id should not be less than 1")
    private Integer categoryId;
}
