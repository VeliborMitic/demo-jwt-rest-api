package com.primer.demojwtrestapi.model.product;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProductGetModel extends ProductBaseModel{

    private Integer id;
}
