package com.primer.demojwtrestapi.controller;

import com.primer.demojwtrestapi.entity.Category;
import com.primer.demojwtrestapi.entity.Product;
import com.primer.demojwtrestapi.exception.ErrorResponse;
import com.primer.demojwtrestapi.exception.FieldError;
import com.primer.demojwtrestapi.model.category.CategoryGetModel;
import com.primer.demojwtrestapi.model.category.CategoryPostModel;
import com.primer.demojwtrestapi.model.product.ProductGetModel;
import com.primer.demojwtrestapi.service.CategoryService;
import com.primer.demojwtrestapi.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    private CategoryService categoryService;
    private ProductService productService;
    private ModelMapper modelMapper;

    private static final String NOT_FOUND = "Resource not found";
    private static final String SERVICE_NOT_AVAILABLE = "The service is not available right now";
    private static final String ACCESS_DENIED = "Access Denied";
    private static final String INVALID_OBJECT = "Invalid object received";
    private static final String NAME_FIELD = "Name";
    private static final String ERROR_OBJECT = "Object with already existing name received";

    @Autowired
    public CategoryController(CategoryService categoryService,
                              ProductService productService,
                              ModelMapper modelMapper) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.modelMapper = modelMapper;
    }

    @ApiOperation(value = "Get a Page of Categoies", response = CategoryGetModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get a page of Categories"),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND)
    })
    @GetMapping
    public HttpEntity get(Pageable pageable) {

        Optional<Page<Category>> optionalPage = categoryService.get(pageable);

        return optionalPage.<HttpEntity>map(
                page -> {
                    List<CategoryGetModel> categories = page.stream()
                            .map(c -> modelMapper.map(c, CategoryGetModel.class))
                            .collect(Collectors.toList());

                    return new ResponseEntity<>(new PageImpl<>(categories, page.getPageable(), page.getTotalElements()), HttpStatus.OK);
                })
                .orElseGet(() -> new ResponseEntity<>(new PageImpl(new ArrayList(), pageable, 0), HttpStatus.OK));
    }

    @ApiOperation(value = "Get Category By Id", response = CategoryGetModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Existing Category"),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND)
    })
    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<Category> optional = categoryService.get(id);

        return optional.<HttpEntity>map(c -> ResponseEntity.ok(modelMapper.map(c, CategoryGetModel.class)))
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(NOT_FOUND), HttpStatus.NOT_FOUND));
    }

    @ApiOperation(value = "Create new Category", response = CategoryGetModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created Category"),
            @ApiResponse(code = 400, message = INVALID_OBJECT),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND),
            @ApiResponse(code = 502, message = SERVICE_NOT_AVAILABLE)
    })
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public HttpEntity create(@Valid @RequestBody CategoryPostModel categoryPostModel, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors().stream()
                    .map(fieldError -> new FieldError(fieldError.getField(), fieldError.getRejectedValue(), fieldError.getDefaultMessage()))
                    .collect(Collectors.toList());

            return new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT, fieldErrors), HttpStatus.BAD_REQUEST);
        }

        Category mapped = modelMapper.map(categoryPostModel, Category.class);

        Optional<Category> queriedByName = categoryService.getByName(mapped.getName());
        if (queriedByName.isPresent()) {
            List<FieldError> fieldErrors = Collections.singletonList(new FieldError(NAME_FIELD, categoryPostModel.getName(), ERROR_OBJECT));
            return new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT, fieldErrors), HttpStatus.CONFLICT);
        }

        Optional<Category> optional = categoryService.create(mapped);

        return optional.<HttpEntity>map(c -> new ResponseEntity<>(modelMapper.map(c, CategoryGetModel.class), HttpStatus.CREATED))
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(SERVICE_NOT_AVAILABLE), HttpStatus.BAD_GATEWAY));
    }

    @ApiOperation(value = "Update Category", response = CategoryGetModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated Category"),
            @ApiResponse(code = 400, message = INVALID_OBJECT),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND),
            @ApiResponse(code = 502, message = SERVICE_NOT_AVAILABLE)
    })
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable("id") Integer id,
                             @Valid @RequestBody CategoryPostModel categoryPostModel) {
        Optional<Category> optional = categoryService.get(id);

        if (!optional.isPresent()) {
            return new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT), HttpStatus.NOT_FOUND);
        }

        if (categoryService.existsByNameAndDifferentId(categoryPostModel.getName(), id)) {
            return new ResponseEntity<>(new FieldError(categoryPostModel.getName(), categoryPostModel, ERROR_OBJECT), HttpStatus.CONFLICT);
        }

        Category mapped = modelMapper.map(categoryPostModel, Category.class);
        mapped.setId(id);

        optional = categoryService.update(mapped);

        return optional.<HttpEntity>map(c -> ResponseEntity.ok(modelMapper.map(c, CategoryGetModel.class)))
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT), HttpStatus.BAD_REQUEST));
    }

    @ApiOperation(value = "Delete Category By Id", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted Category"),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND)
    })
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<Category> optional = categoryService.get(id);

        return optional.<HttpEntity>map(
                c -> {
                    categoryService.delete(c);
                    return new ResponseEntity<>("Category wit Id \"" + c.getId() + "\" and name \""
                            + c.getName() + "\" succesfuly deleted!", HttpStatus.OK);
                })
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(NOT_FOUND), HttpStatus.NOT_FOUND));
    }

    @ApiOperation(value = "Get a Page of Products By Category", response = CategoryGetModel.class)
    @GetMapping("/{id}/products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get a page of Product by category"),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND)
    })
    public HttpEntity get(@PathVariable("id") Integer id, Pageable pageable) {

        Optional<Category> optionalCategory = categoryService.get(id);
        if (!optionalCategory.isPresent()) {
            return new ResponseEntity<>(new ErrorResponse(NOT_FOUND), HttpStatus.NOT_FOUND);
        }

        Optional<Page<Product>> optionalPage = productService.getAllByCategoryId(id, pageable);

        return optionalPage.map(
                page -> {
                    List<ProductGetModel> products = page.stream()
                            .map(p -> modelMapper.map(p, ProductGetModel.class))
                            .collect(Collectors.toList());

                    return new ResponseEntity<>(new PageImpl<>(products, page.getPageable(),
                            page.getTotalElements()), HttpStatus.OK);
                })
                .orElseGet(() -> ResponseEntity.ok(new PageImpl(new ArrayList(), pageable, 0)));
    }
}
