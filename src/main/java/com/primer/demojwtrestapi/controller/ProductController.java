package com.primer.demojwtrestapi.controller;

import com.primer.demojwtrestapi.entity.Category;
import com.primer.demojwtrestapi.entity.Product;
import com.primer.demojwtrestapi.exception.ErrorResponse;
import com.primer.demojwtrestapi.exception.FieldError;
import com.primer.demojwtrestapi.model.product.ProductGetModel;
import com.primer.demojwtrestapi.model.product.ProductPostModel;
import com.primer.demojwtrestapi.service.CategoryService;
import com.primer.demojwtrestapi.service.ProductService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    private ProductService productService;
    private CategoryService categoryService;
    private ModelMapper modelMapper;

    private static final String NOT_FOUND = "Resource not found";
    private static final String SERVICE_NOT_AVAILABLE = "The service is not available right now";
    private static final String ACCESS_DENIED = "Access Denied";
    private static final String INVALID_OBJECT = "Invalid object received";
    private static final String NAME_FIELD = "Name";
    private static final String CATEGORY_ID = "Category Id";
    private static final String ERROR_OBJECT = "Object with already existing name received";

    @Autowired
    public ProductController(ProductService productService, CategoryService categoryService, ModelMapper modelMapper) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get a page of Products"),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND)
    })
    @GetMapping
    public HttpEntity get(Pageable pageable) {
        Optional<Page<Product>> optionalPage = productService.get(pageable);

        return optionalPage.map(
                page -> {
                    List<ProductGetModel> products = page.stream()
                            .map(p -> modelMapper.map(p, ProductGetModel.class))
                            .collect(Collectors.toList());

                    return new ResponseEntity<>(new PageImpl<>(products, page.getPageable(), page.getTotalElements()), HttpStatus.OK);
                })
                .orElseGet(() -> new ResponseEntity<>(new PageImpl(new ArrayList(), pageable, 0), HttpStatus.OK));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Existing Product"),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND)
    })
    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<Product> optional = productService.get(id);

        return optional.map(p -> ResponseEntity.ok(modelMapper.map(p, ProductGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created Product"),
            @ApiResponse(code = 400, message = INVALID_OBJECT),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND),
            @ApiResponse(code = 502, message = SERVICE_NOT_AVAILABLE)
    })
    @PostMapping
    public HttpEntity create(@Valid @RequestBody ProductPostModel productPostModel,
                             BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors().stream()
                    .map(fieldError -> new FieldError(fieldError.getField(), fieldError.getRejectedValue(), fieldError.getDefaultMessage()))
                    .collect(Collectors.toList());

            return new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT, fieldErrors), HttpStatus.BAD_REQUEST);
        }

        Product mapped = modelMapper.map(productPostModel, Product.class);
        mapped.setId(null);

        Optional<Product> queriedByName = productService.getByName(mapped.getName());
        if (queriedByName.isPresent()) {
            List<FieldError> fieldErrors = Collections.singletonList(new FieldError(NAME_FIELD, productPostModel.getName(), ERROR_OBJECT));
            return new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT, fieldErrors), HttpStatus.CONFLICT);
        }

        Optional<Category> optionalCategory = categoryService.get(productPostModel.getCategoryId());
        if (!optionalCategory.isPresent()) {
            return new ResponseEntity<>(NOT_FOUND, HttpStatus.NOT_FOUND);
        }

        mapped.setCategory(optionalCategory.get());

        Optional<Product> optional = productService.create(mapped);

        return optional.<HttpEntity>map(p -> new ResponseEntity<>(modelMapper.map(p, ProductGetModel.class), HttpStatus.CREATED))
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(SERVICE_NOT_AVAILABLE), HttpStatus.BAD_GATEWAY));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated Product"),
            @ApiResponse(code = 400, message = INVALID_OBJECT),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND),
            @ApiResponse(code = 502, message = SERVICE_NOT_AVAILABLE)
    })
    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable("id") Integer id,
                             @Valid @RequestBody ProductPostModel productPostModel,
                             BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors().stream()
                    .map(fieldError -> new FieldError(fieldError.getField(), fieldError.getRejectedValue(), fieldError.getDefaultMessage()))
                    .collect(Collectors.toList());

            return new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT, fieldErrors), HttpStatus.BAD_REQUEST);
        }

        Optional<Product> optionalProduct = productService.get(id);
        if (!optionalProduct.isPresent()) {
            return new ResponseEntity<>(new ErrorResponse(NOT_FOUND), HttpStatus.NOT_FOUND);
        }

        Optional<Category> optionalCategory = categoryService.get(productPostModel.getCategoryId());
        if (!optionalCategory.isPresent()) {
            List<FieldError> fieldErrors = Collections.singletonList(new FieldError(CATEGORY_ID, productPostModel.getName(), ERROR_OBJECT));
            return new ResponseEntity<>(new ErrorResponse(INVALID_OBJECT, fieldErrors), HttpStatus.CONFLICT);

        }

        Product mapped = modelMapper.map(productPostModel, Product.class);
        mapped.setId(id);
        mapped.setCategory(optionalCategory.get());

        Optional<Product> optionalUpdated = productService.update(mapped);

        return optionalUpdated.<HttpEntity>map(p -> new ResponseEntity<>(modelMapper.map(p, ProductGetModel.class), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(SERVICE_NOT_AVAILABLE), HttpStatus.BAD_GATEWAY));
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted Product"),
            @ApiResponse(code = 403, message = ACCESS_DENIED),
            @ApiResponse(code = 404, message = NOT_FOUND)
    })
    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<Product> optional = productService.get(id);

        return optional.<HttpEntity>map(
                p -> {
                    productService.delete(p);
                    return new ResponseEntity<>("Category wit Id \"" + p.getId() + "\" and name \""
                            + p.getName() + "\" succesfuly deleted!", HttpStatus.OK);
                })
                .orElseGet(() -> new ResponseEntity<>(new ErrorResponse(NOT_FOUND), HttpStatus.NOT_FOUND));
    }
}
