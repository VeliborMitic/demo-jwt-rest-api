package com.primer.demojwtrestapi.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.primer.demojwtrestapi.configuration.JsonWebTokenConfiguration;
import com.primer.demojwtrestapi.exception.ErrorResponse;
import com.primer.demojwtrestapi.security.CredentialsNotFoundException;
import com.primer.demojwtrestapi.security.JsonWebTokenProvider;
import com.primer.demojwtrestapi.security.UserCredentials;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private ObjectMapper objectMapper;
    private JsonWebTokenProvider jsonWebTokenProvider;
    private AuthenticationManager authenticationManager;
    private JsonWebTokenConfiguration configuration;

    public JwtAuthenticationFilter(ObjectMapper objectMapper,
                                   JsonWebTokenProvider jsonWebTokenProvider,
                                   AuthenticationManager authenticationManager,
                                   JsonWebTokenConfiguration configuration) {
        this.objectMapper = objectMapper;
        this.jsonWebTokenProvider = jsonWebTokenProvider;
        this.authenticationManager = authenticationManager;
        this.configuration = configuration;

        setAuthenticationEntryPoint();
    }

    private void setAuthenticationEntryPoint() {
        String authEntryPoint = configuration.getEntryPoint();
        if (authEntryPoint != null)
            this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(
                    authEntryPoint,
                    "POST"
            ));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            UserCredentials userCredentials = objectMapper.readValue(request.getInputStream(), UserCredentials.class);
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    userCredentials.getEmail(),
                    userCredentials.getPassword());

            return authenticationManager.authenticate(authToken);
        } catch (IOException e) {
            throw new CredentialsNotFoundException("Credentials are not present.");
        } catch (AuthenticationException e) {
            throw new BadCredentialsException(e.getMessage(), e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) {
        String jwt = String.format("%s %s",
                configuration.getType(),
                jsonWebTokenProvider.generateToken(authResult));
        response.addHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader(configuration.getHeader(), jwt);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        response.setStatus(401);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        ErrorResponse errorResponse = new ErrorResponse(failed.getMessage());
        writer.print(objectMapper.writeValueAsString(errorResponse));
        writer.flush();
    }
}
