package com.primer.demojwtrestapi.exception;

import lombok.Data;

import java.util.List;

@Data
public class ErrorResponse {
    private String message;
    private List<FieldError> errors;

    public ErrorResponse(String message, List<FieldError> errors) {
        this.message = message;
        this.errors = errors;
    }

    public ErrorResponse(String message) {
        this.message = message;
    }


}