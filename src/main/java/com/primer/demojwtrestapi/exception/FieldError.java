package com.primer.demojwtrestapi.exception;

import lombok.Data;

@Data
public class FieldError {
    private final String field;
    private final Object rejectedValue;
    private final String message;
}