package com.primer.demojwtrestapi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BaseService<T> {

    List<T> get();

    Optional<Page<T>> get(Pageable pageable);

    Optional<T> get(Integer id);

    Optional<T> create(T entity);

    Optional<T> update(T entity);

    void delete(Integer id);

    void delete(T t);
}