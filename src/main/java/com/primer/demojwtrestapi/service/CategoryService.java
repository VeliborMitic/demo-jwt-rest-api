package com.primer.demojwtrestapi.service;

import com.primer.demojwtrestapi.entity.Category;

import java.util.Optional;

public interface CategoryService extends BaseService<Category> {

    Optional<Category> getByName(String name);

    boolean existsByNameAndDifferentId(String name, Integer id);
}
