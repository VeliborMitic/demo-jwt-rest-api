package com.primer.demojwtrestapi.service.impl;

import com.primer.demojwtrestapi.entity.BaseEntity;
import com.primer.demojwtrestapi.repository.BaseRepository;
import com.primer.demojwtrestapi.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public abstract class BaseServiceImpl<T extends BaseEntity> implements BaseService<T> {

    private BaseRepository<T> baseRepository;

    public BaseServiceImpl(BaseRepository<T> baseRepository) {
        this.baseRepository = baseRepository;
    }

    @Override
    public List<T> get(){
        return baseRepository.findAll();
    }

    @Override
    public Optional<Page<T>> get(Pageable pageable) {
        return Optional.of(baseRepository.findAll(pageable));
    }

    @Override
    public Optional<T> get(Integer id) {
        return baseRepository.findById(id);
    }

    @Override
    public Optional<T> create(T entity) {
        return Optional.of(baseRepository.save(entity));
    }

    @Override
    public Optional<T> update(T entity){
        if (get(entity.getId()).isPresent()) {
            return Optional.of(baseRepository.save(entity));
        }

        return Optional.empty();
    }

    @Override
    public void delete(Integer id) {
        baseRepository.deleteById(id);
    }

    public void delete(T t){baseRepository.delete(t);
    }
}
