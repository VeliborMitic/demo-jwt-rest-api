package com.primer.demojwtrestapi.service.impl;

import com.primer.demojwtrestapi.entity.Category;
import com.primer.demojwtrestapi.repository.CategoryRepository;
import com.primer.demojwtrestapi.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryServiceImpl extends BaseServiceImpl<Category> implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        super(categoryRepository);
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Optional<Category> getByName(String name) {
        return categoryRepository.findByName(name);
    }

    @Override
    public boolean existsByNameAndDifferentId(String name, Integer id) {
        return categoryRepository.existsByNameAndDifferentId(name, id);
    }
}