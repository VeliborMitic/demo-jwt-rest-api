package com.primer.demojwtrestapi.service.impl;

import com.primer.demojwtrestapi.entity.Product;
import com.primer.demojwtrestapi.repository.ProductRepository;
import com.primer.demojwtrestapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        super(productRepository);
        this.productRepository = productRepository;
    }

    @Override
    public Optional<Product> getByName(String name) {
        return productRepository.findByName(name);
    }

    @Override
    public List<Product> getAllByCategoryId(Integer id) {
        return productRepository.findAllByCategoryId(id);
    }

    @Override
    public Optional<Page<Product>> getAllByCategoryId(Integer id, Pageable pageable) {
        return Optional.of(productRepository.findAllByCategoryId(id, pageable));
    }
}