package com.primer.demojwtrestapi.service;

import com.primer.demojwtrestapi.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ProductService extends BaseService<Product> {

    Optional<Product> getByName(String name);

    List<Product> getAllByCategoryId(Integer id);

    Optional<Page<Product>> getAllByCategoryId(Integer id, Pageable pageable);
}
