INSERT INTO roles (name)
VALUES ('ROLE_ADMIN'),
       ('ROLE_USER');

INSERT INTO users (email, name, password, role_id)
VALUES ('admin@jwtdemo.com', 'Demo Admin', '$2a$11$1pbNJ3WA56JGLSxeRkQ2DONJZoUMfHqkH0KQSwMChqA2Lt5yC76g6', 1),
       ('user@jwtdemo.com', 'Demo User', '$2a$11$1pbNJ3WA56JGLSxeRkQ2DONJZoUMfHqkH0KQSwMChqA2Lt5yC76g6', 2);

INSERT INTO categories (name) VALUES ('aaaa bbbb');
INSERT INTO categories (name) VALUES ('cececece');
INSERT INTO categories (name) VALUES ('dede dede');
INSERT INTO categories (name) VALUES ('ffff gggg');
INSERT INTO categories (name) VALUES ('hahahaha');
INSERT INTO categories (name) VALUES ('iiii jjjj');
INSERT INTO categories (name) VALUES ('kkkk llll');
INSERT INTO categories (name) VALUES ('mmmm nnnn');
INSERT INTO categories (name) VALUES ('ooooooooo');
INSERT INTO categories (name) VALUES ('pipipipipi');
INSERT INTO categories (name) VALUES ('qjuqjuqju');
INSERT INTO categories (name) VALUES ('uuuuuuuuu');
INSERT INTO categories (name) VALUES ('xxxx yyyy');
INSERT INTO categories (name) VALUES ('zedzedzed');
INSERT INTO categories (name) VALUES ('vvvv wwww');

INSERT INTO products (name, category_id) VALUES ('p1', '1');
INSERT INTO products (name, category_id) VALUES ('p2', '2');
INSERT INTO products (name, category_id) VALUES ('p3', '3');
INSERT INTO products (name, category_id) VALUES ('p4', '3');
INSERT INTO products (name, category_id) VALUES ('p5', '4');
INSERT INTO products (name, category_id) VALUES ('p6', '4');
INSERT INTO products (name, category_id) VALUES ('p7', '4');
INSERT INTO products (name, category_id) VALUES ('p8', '5');
INSERT INTO products (name, category_id) VALUES ('p9', '5');
INSERT INTO products (name, category_id) VALUES ('p10', '5');
INSERT INTO products (name, category_id) VALUES ('p11', '5');
INSERT INTO products (name, category_id) VALUES ('p12', '6');
INSERT INTO products (name, category_id) VALUES ('p13', '6');
INSERT INTO products (name, category_id) VALUES ('p14', '7');
INSERT INTO products (name, category_id) VALUES ('p15', '7');
INSERT INTO products (name, category_id) VALUES ('p16', '7');
INSERT INTO products (name, category_id) VALUES ('p17', '7');
INSERT INTO products (name, category_id) VALUES ('p18', '8');
INSERT INTO products (name, category_id) VALUES ('p19', '8');
INSERT INTO products (name, category_id) VALUES ('p20', '9');
INSERT INTO products (name, category_id) VALUES ('p21', '10');
INSERT INTO products (name, category_id) VALUES ('p22', '11');
INSERT INTO products (name, category_id) VALUES ('p23', '12');
INSERT INTO products (name, category_id) VALUES ('p24', '13');
INSERT INTO products (name, category_id) VALUES ('p25', '14');

