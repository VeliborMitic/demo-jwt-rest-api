FROM openjdk:8-jdk-alpine
WORKDIR /code
COPY . /code
RUN ["chmod", "+x", "./mvnw"]
RUN ./mvnw package -Dmaven.test.skip=true
RUN cp target/demo-jwr-rest-api-0.0.1-SNAPSHOT.jar target/app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/code/target/app.jar"]
